<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../database.php';


$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);


function getUrlOverviewFiles($idcourse) {
    global $db;
    $query = $db->query("select c.id, file.filename, cont.id as contexte from mdl_files file
inner join mdl_context cont
on file.contextid = cont.id
inner join mdl_course c
on c.id = cont.instanceid
where file.component = 'course'
and c.id = '". $idcourse ."' limit 1");
//$query->execute(array((int) $idcourse));
    $data = $query->fetch();
    return "/pluginfile.php/".$data['contexte']."/course/overviewfiles/".$data['filename'];
}

function getCategories($libCategorie) {
    global $db;
    $query = $db->query("SELECT * FROM mdl_course WHERE category = (SELECT id FROM mdl_course_categories WHERE name = '". $libCategorie ."')");
    return $query->fetchAll();
}



$elearningCaegories = getCategories('elearning');
$microvideosCategories = getCategories('microlearning');
$defaultUrl = "https://itsocial.fr/wp-content/uploads/2018/05/iStock-869155894.png";
/*$query = $db->query("SELECT * FROM mdl_course WHERE category = 'elearning'");

$query2 = $db->query("SELECT * FROM mdl_course WHERE category = 'microlearning'");
var_dump($query2->fetchAll());*/


//var_dump(getCategories('elearning'));

?>
<!DOCTYPE html>

<html  dir="ltr" lang="en" xml:lang="en">
<head>

    <title>ISO Learning</title>
    <link rel="shortcut icon" href="//learning.iso.org/pluginfile.php/1/theme_space/favicon/1601276563/Logo%20copie.ico" />






    <script src="https://learning.iso.org/theme/space/addons/overlayscrollbars/OverlayScrollbars.min.js"></script>

    <script src="https://learning.iso.org/theme/space/addons/tinyslider/tiny-slider.js"></script>
    <link rel="stylesheet" href="https://learning.iso.org/theme/space/addons/tinyslider/tiny-slider.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="moodle, ISO Learning" />
    <link rel="stylesheet" type="text/css" href="https://learning.iso.org/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css" /><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://learning.iso.org/theme/styles.php/space/1601276563_1/all" />
    <script>
        //<![CDATA[
        var M = {}; M.yui = {};
        M.pageloadstarttime = new Date();
        M.cfg = {"wwwroot":"https:\/\/learning.iso.org","sesskey":"Hvlarp0ak6","sessiontimeout":"7200","themerev":"1601276563","slasharguments":1,"theme":"space","iconsystemmodule":"core\/icon_system_fontawesome","jsrev":"1601276563","admin":"admin","svgicons":true,"usertimezone":"Europe\/London","contextid":2,"langrev":1602127087,"templaterev":"1601276563"};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type='css';me.path=me.path.replace(/\.js/,'.css');me.path=me.path.replace(/\/yui2-skin/,'/assets/skins/sam/yui2-skin')}};
        var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,'').split('-'),component=parts.shift(),module=parts[0],min='-min';if(/-(skin|core)$/.test(me.name)){parts.pop();me.type='css';min=''}
            if(module){var filename=parts.join('-');me.path=component+'/'+module+'/'+filename+min+'.'+me.type}else{me.path=component+'/'+component+'.'+me.type}};
        YUI_config = {"debug":false,"base":"https:\/\/learning.iso.org\/lib\/yuilib\/3.17.2\/","comboBase":"https:\/\/learning.iso.org\/theme\/yui_combo.php?","combine":true,"filter":null,"insertBefore":"firstthemesheet","groups":{"yui2":{"base":"https:\/\/learning.iso.org\/lib\/yuilib\/2in3\/2.9.0\/build\/","comboBase":"https:\/\/learning.iso.org\/theme\/yui_combo.php?","combine":true,"ext":false,"root":"2in3\/2.9.0\/build\/","patterns":{"yui2-":{"group":"yui2","configFn":yui1ConfigFn}}},"moodle":{"name":"moodle","base":"https:\/\/learning.iso.org\/theme\/yui_combo.php?m\/1601276563\/","combine":true,"comboBase":"https:\/\/learning.iso.org\/theme\/yui_combo.php?","ext":false,"root":"m\/1601276563\/","patterns":{"moodle-":{"group":"moodle","configFn":yui2ConfigFn}},"filter":null,"modules":{"moodle-core-languninstallconfirm":{"requires":["base","node","moodle-core-notification-confirm","moodle-core-notification-alert"]},"moodle-core-notification":{"requires":["moodle-core-notification-dialogue","moodle-core-notification-alert","moodle-core-notification-confirm","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-core-notification-dialogue":{"requires":["base","node","panel","escape","event-key","dd-plugin","moodle-core-widget-focusafterclose","moodle-core-lockscroll"]},"moodle-core-notification-alert":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-confirm":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-exception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-ajaxexception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-dragdrop":{"requires":["base","node","io","dom","dd","event-key","event-focus","moodle-core-notification"]},"moodle-core-chooserdialogue":{"requires":["base","panel","moodle-core-notification"]},"moodle-core-popuphelp":{"requires":["moodle-core-tooltip"]},"moodle-core-formchangechecker":{"requires":["base","event-focus","moodle-core-event"]},"moodle-core-maintenancemodetimer":{"requires":["base","node"]},"moodle-core-handlebars":{"condition":{"trigger":"handlebars","when":"after"}},"moodle-core-blocks":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification"]},"moodle-core-lockscroll":{"requires":["plugin","base-build"]},"moodle-core-tooltip":{"requires":["base","node","io-base","moodle-core-notification-dialogue","json-parse","widget-position","widget-position-align","event-outside","cache-base"]},"moodle-core-actionmenu":{"requires":["base","event","node-event-simulate"]},"moodle-core-event":{"requires":["event-custom"]},"moodle-core_availability-form":{"requires":["base","node","event","event-delegate","panel","moodle-core-notification-dialogue","json"]},"moodle-backup-confirmcancel":{"requires":["node","node-event-simulate","moodle-core-notification-confirm"]},"moodle-backup-backupselectall":{"requires":["node","event","node-event-simulate","anim"]},"moodle-course-management":{"requires":["base","node","io-base","moodle-core-notification-exception","json-parse","dd-constrain","dd-proxy","dd-drop","dd-delegate","node-event-delegate"]},"moodle-course-categoryexpander":{"requires":["node","event-key"]},"moodle-course-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-course-coursebase","moodle-course-util"]},"moodle-course-formatchooser":{"requires":["base","node","node-event-simulate"]},"moodle-course-util":{"requires":["node"],"use":["moodle-course-util-base"],"submodules":{"moodle-course-util-base":{},"moodle-course-util-section":{"requires":["node","moodle-course-util-base"]},"moodle-course-util-cm":{"requires":["node","moodle-course-util-base"]}}},"moodle-course-modchooser":{"requires":["moodle-core-chooserdialogue","moodle-course-coursebase"]},"moodle-form-shortforms":{"requires":["node","base","selector-css3","moodle-core-event"]},"moodle-form-dateselector":{"requires":["base","node","overlay","calendar"]},"moodle-form-passwordunmask":{"requires":[]},"moodle-question-chooser":{"requires":["moodle-core-chooserdialogue"]},"moodle-question-preview":{"requires":["base","dom","event-delegate","event-key","core_question_engine"]},"moodle-question-searchform":{"requires":["base","node"]},"moodle-availability_completion-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_date-form":{"requires":["base","node","event","io","moodle-core_availability-form"]},"moodle-availability_grade-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_group-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_grouping-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_profile-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-mod_assign-history":{"requires":["node","transition"]},"moodle-mod_quiz-toolboxes":{"requires":["base","node","event","event-key","io","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-slot","moodle-core-notification-ajaxexception"]},"moodle-mod_quiz-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-base","moodle-mod_quiz-util-page","moodle-mod_quiz-util-slot","moodle-course-util"]},"moodle-mod_quiz-modform":{"requires":["base","node","event"]},"moodle-mod_quiz-util":{"requires":["node","moodle-core-actionmenu"],"use":["moodle-mod_quiz-util-base"],"submodules":{"moodle-mod_quiz-util-base":{},"moodle-mod_quiz-util-slot":{"requires":["node","moodle-mod_quiz-util-base"]},"moodle-mod_quiz-util-page":{"requires":["node","moodle-mod_quiz-util-base"]}}},"moodle-mod_quiz-quizbase":{"requires":["base","node"]},"moodle-mod_quiz-autosave":{"requires":["base","node","event","event-valuechange","node-event-delegate","io-form"]},"moodle-mod_quiz-questionchooser":{"requires":["moodle-core-chooserdialogue","moodle-mod_quiz-util","querystring-parse"]},"moodle-message_airnotifier-toolboxes":{"requires":["base","node","io"]},"moodle-filter_glossary-autolinker":{"requires":["base","node","io-base","json-parse","event-delegate","overlay","moodle-core-event","moodle-core-notification-alert","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-filter_mathjaxloader-loader":{"requires":["moodle-core-event"]},"moodle-editor_atto-rangy":{"requires":[]},"moodle-editor_atto-editor":{"requires":["node","transition","io","overlay","escape","event","event-simulate","event-custom","node-event-html5","node-event-simulate","yui-throttle","moodle-core-notification-dialogue","moodle-core-notification-confirm","moodle-editor_atto-rangy","handlebars","timers","querystring-stringify"]},"moodle-editor_atto-plugin":{"requires":["node","base","escape","event","event-outside","handlebars","event-custom","timers","moodle-editor_atto-menu"]},"moodle-editor_atto-menu":{"requires":["moodle-core-notification-dialogue","node","event","event-custom"]},"moodle-report_eventlist-eventfilter":{"requires":["base","event","node","node-event-delegate","datatable","autocomplete","autocomplete-filters"]},"moodle-report_loglive-fetchlogs":{"requires":["base","event","node","io","node-event-delegate"]},"moodle-gradereport_grader-gradereporttable":{"requires":["base","node","event","handlebars","overlay","event-hover"]},"moodle-gradereport_history-userselector":{"requires":["escape","event-delegate","event-key","handlebars","io-base","json-parse","moodle-core-notification-dialogue"]},"moodle-tool_capability-search":{"requires":["base","node"]},"moodle-tool_lp-dragdrop-reorder":{"requires":["moodle-core-dragdrop"]},"moodle-tool_monitor-dropdown":{"requires":["base","event","node"]},"moodle-assignfeedback_editpdf-editor":{"requires":["base","event","node","io","graphics","json","event-move","event-resize","transition","querystring-stringify-simple","moodle-core-notification-dialog","moodle-core-notification-alert","moodle-core-notification-warning","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-atto_accessibilitychecker-button":{"requires":["color-base","moodle-editor_atto-plugin"]},"moodle-atto_accessibilityhelper-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_align-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_bold-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_charmap-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_clear-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_collapse-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_emojipicker-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_emoticon-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_equation-button":{"requires":["moodle-editor_atto-plugin","moodle-core-event","io","event-valuechange","tabview","array-extras"]},"moodle-atto_h5p-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_html-beautify":{},"moodle-atto_html-codemirror":{"requires":["moodle-atto_html-codemirror-skin"]},"moodle-atto_html-button":{"requires":["promise","moodle-editor_atto-plugin","moodle-atto_html-beautify","moodle-atto_html-codemirror","event-valuechange"]},"moodle-atto_image-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_indent-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_italic-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_link-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-usedfiles":{"requires":["node","escape"]},"moodle-atto_managefiles-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_media-button":{"requires":["moodle-editor_atto-plugin","moodle-form-shortforms"]},"moodle-atto_noautolink-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_orderedlist-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_recordrtc-recording":{"requires":["moodle-atto_recordrtc-button"]},"moodle-atto_recordrtc-button":{"requires":["moodle-editor_atto-plugin","moodle-atto_recordrtc-recording"]},"moodle-atto_rtl-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_strike-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_subscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_superscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_table-button":{"requires":["moodle-editor_atto-plugin","moodle-editor_atto-menu","event","event-valuechange"]},"moodle-atto_title-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_underline-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_undo-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_unorderedlist-button":{"requires":["moodle-editor_atto-plugin"]}}},"gallery":{"name":"gallery","base":"https:\/\/learning.iso.org\/lib\/yuilib\/gallery\/","combine":true,"comboBase":"https:\/\/learning.iso.org\/theme\/yui_combo.php?","ext":false,"root":"gallery\/1601276563\/","patterns":{"gallery-":{"group":"gallery"}}}},"modules":{"core_filepicker":{"name":"core_filepicker","fullpath":"https:\/\/learning.iso.org\/lib\/javascript.php\/1601276563\/repository\/filepicker.js","requires":["base","node","node-event-simulate","json","async-queue","io-base","io-upload-iframe","io-form","yui2-treeview","panel","cookie","datatable","datatable-sort","resize-plugin","dd-plugin","escape","moodle-core_filepicker","moodle-core-notification-dialogue"]},"core_comment":{"name":"core_comment","fullpath":"https:\/\/learning.iso.org\/lib\/javascript.php\/1601276563\/comment\/comment.js","requires":["base","io-base","node","json","yui2-animation","overlay","escape"]},"mathjax":{"name":"mathjax","fullpath":"https:\/\/cdn.jsdelivr.net\/npm\/mathjax@2.7.8\/MathJax.js?delayStartupUntil=configured"}}};
        M.yui.loader = {modules: {}};

        //]]>
    </script>
    <meta name="description" content="ISO Digital Learning Platform" />
    <meta name="description" content="ISO Digital Learning Platform" />


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


    <style type="text/css">
        @font-face {
            font-family: MetaCompPro-Normal;
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontlighteot/1601276563/MetaWebPro-Thin.eot');
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontlighteot/1601276563/MetaWebPro-Thin.eot?#iefix') format('embedded-opentype'),
            url('') format('woff2'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontlightwoff/1601276563/MetaWebPro-Thin.woff') format('woff'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontlightttf/1601276563/MetaCompPro-Thin.ttf') format('truetype'),
            url('#MetaCompPro-Thin') format('svg');
            font-weight: 300;
            font-style: normal;
        }
        @font-face {
            font-family: MetaCompPro-Normal;
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontregulareot/1601276563/MetaWebPro-Normal.eot');
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontregulareot/1601276563/MetaWebPro-Normal.eot?#iefix') format('embedded-opentype'),
            url('') format('woff2'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontregularwoff/1601276563/MetaWebPro-Normal.woff') format('woff'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontregularttf/1601276563/MetaCompPro-Normal.ttf') format('truetype'),
            url('#MetaCompPro-Normal') format('svg');
            font-weight: 400;
            font-style: normal;
        }
        @font-face {
            font-family: MetaCompPro-Normal;
            src: url('');
            src: url('?#iefix') format('embedded-opentype'),
            url('') format('woff2'),
            url('') format('woff'),
            url('') format('truetype'),
            url('#') format('svg');
            font-weight: 500;
            font-style: normal;
        }

        @font-face {
            font-family: MetaCompPro-Normal;
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontboldeot/1601276563/MetaWebPro-Bold.eot');
            src: url('//learning.iso.org/pluginfile.php/1/theme_space/customfontboldeot/1601276563/MetaWebPro-Bold.eot?#iefix') format('embedded-opentype'),
            url('') format('woff2'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontboldwoff/1601276563/MetaWebPro-Bold.woff') format('woff'),
            url('//learning.iso.org/pluginfile.php/1/theme_space/customfontboldttf/1601276563/MetaScCompPro-Bold.ttf') format('truetype'),
            url('#MetaCompPro-Bold') format('svg');
            font-weight: 700;
            font-style: normal;
        }
    </style>

</head>
<body  id="page-site-index" class="format-site course path-site chrome dir-ltr lang-en yui-skin-sam yui3-skin-sam learning-iso-org pagelayout-frontpage course-1 context-2 notloggedin ">

<div id="page-wrapper">

    <script src="https://learning.iso.org/lib/javascript.php/1601276563/lib/babel-polyfill/polyfill.min.js"></script>
    <script src="https://learning.iso.org/lib/javascript.php/1601276563/lib/mdn-polyfills/polyfill.js"></script>
    <script src="https://learning.iso.org/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script><script src="https://learning.iso.org/theme/jquery.php/core/jquery-3.4.1.min.js"></script>
    <script src="https://learning.iso.org/lib/javascript.php/1601276563/lib/javascript-static.js"></script>
    <script>
        //<![CDATA[
        document.body.className += ' jsenabled';
        //]]>
    </script>



    <div class="c-logo-mobile d-md-none text-center">
        <a href="https://learning.iso.org" class="navbar-brand  d-none d-sm-inline">




            <h1 class=" site-name text-center">ISO Learning</h1>

        </a>
    </div>


    <div id="topBar1" class="s-top s-top--sidebar justify-content-center ">


        <div class="d-sm-inline d-md-none">
            <button type="button" class="sidebar-btn" aria-expanded="false" aria-controls="nav-drawer" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav">
                <i class="fas fa-equals" id="mobileSidebarBtn"></i>
                <i class="fas fa-times hidden" id="mobileSidebarBtnClose"></i>
            </button>
        </div>

        <div class="d-sm-inline d-md-none">
            <button type="button" class="mobile-topbar-btn">
                <i class="fas fa-th-large" id="mobileTopBarBtn"></i>
                <i class="fas fa-times hidden" id="mobileTopBarCloseBtn"></i>
            </button>
        </div>

        <div id="navBar" class="s-top-container c-container row no-gutters justify-content-start align-items-center">


            <div data-region="drawer-toggle" class="d-sm-none d-md-inline ml-md-0 mr-md-3">
                <button type="button" class="sidebar-btn" aria-expanded="false" aria-controls="nav-drawer" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav">

                    <i class="fas fa-equals" id="sidebarBtn"></i>
                </button>
            </div>


            <div class="c-nav-user nav ml-sm-center ml-md-auto align-items-center justify-content-center">


                <div class="search-box d-block">

                </div>

                <div class="top-icons-nav c-menu-sep mr-md-3"></div>





                <div class="c-user-menu">
                    <div class="usermenu"><span class="login"><a class="login-btn" href="https://learning.iso.org/login/index.php">Log in<i class="ml-2 icon fas fa-sign-in-alt"></i></a></span></div>
                </div>

            </div>

        </div>

    </div>


    <!--<div class="c-top-logo l-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1 col-xs-6 col-sm">
                    <img src="https://learning.iso.org/pluginfile.php/1/core_admin/logo/0x300/1601276563/Logo.png" alt="ISO Learning" width="100px"/>
                </div>

                <div class="col-md-10 mt-2 col-xs-6 col-sm">
                    <h1 class="font-weight-bold" style="font-size: 60px">DIGITAL LEARNING PLATFORM</h1>
                </div>
            </div>



            <div role="main" style="background-color: #f7f7f7; height: 100px;">
                <span id="maincontent"></span>


                <div class="box mdl-align card-content  align-self-center center-block">
                    <form action="https://learning.iso.org/course/search.php" id="coursesearch" method="get" class="form-inline">
                        <fieldset class="coursesearchbox invisiblefieldset m-b-2">-->

                            <!-- <label for="shortsearchbox">Search courses</label> -->
                           <!-- <div class="row align-self-center center-block">
                                <div class="form-group col-lg-12 col-md-12 col-xs-6 d-flex justify-content-center" style="">
                                    <input id="shortsearchbox" name="search" type="text" size="45" value="" class="form-control col-lg-10" placeholder="Search Learning Solutions">
                                    <button class="btn btn-secondary" type="submit">Go</button>
                                </div>
                            </div>


                        </fieldset>
                    </form>
                </div><br />
                <a class="skip-block skip" href="#skipavailablecourses">Skip available courses</a>-->
                <!--<div id="frontpage-available-course-list"><h2>Available courses</h2><div class="s-courses frontpage-course-list-all">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1">
                                <div class='c-course-content row no-gutters'><a href='https://learning.iso.org/course/view.php?id=2' class='col-12 align-self-start p-0'>
                                        <div class='card-img-top myoverviewimg courseimage' style='background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);'></div></a>
                                    <div class="c-course-box-content w-100"><h4 class='course-box-title'><span class='course-box-title-txt' title='Project Management in the ISO environment'><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class='course-box-icons'><ul class='course-box-icons-list'><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class='course-box-content-desc'><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br /></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>: English<br /></p><p><b style="font-size:.85rem;">Delivery method : </b><span style="font-size:.85rem;">Self-paced course</span><br /></p><br /><p></p><br /><p></p></div></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div></div></div><span class="skip-block-to" id="skipavailablecourses"></span><br /></div>-->


                <!--
                    Remove to next
                    class: section-padding
                -->

                <!--<div id="block2" class="s-block-2 l-container s-special-box row no-gutters justify-content-sm-center justify-content-lg-between">

                    <div class="col-12 row no-gutters justify-content-between py-0 px-1">-->

                        <!-- Remove to next
                            class:
                                Remove: my-5
                                Update: p-sm-3 -> p-sm-1, p-md-5 -> p-md-3
                        -->

                       <!-- <div id="block2-1" class=" col-sm-12 col-md-7 p-sm-1 p-md-3">



                            <h3 class="h3 c-block2-title">LEARN ONLINE WITH ISO</h3>
                            <div class="c-special-box-desc"><p>The ISO digital learning platform offers a wide range of digital learning solutions in an effort to provide the learners with a great deal of flexibility, allowing them to study at any time from any place at their own convenient speed without worrying about timetables and schedules.&nbsp;<br>Unlike traditional classroom learning opportunities, online learning environments foster additional learning experiences where learners can interect, collaborate and retain information in a far better manner.&nbsp;<br></p></div>


                        </div>-->
                        <!--
                            Remove to next
                            class:
                                 Remove: my-5
                                 Update: p-sm-3 -> p-sm-1, p-md-5 -> p-md-3
                        -->
                       <!-- <div id="block2-2" class="col-sm-12 col-md-4 p-sm-1 p-md-3">



                            <h3 class="h3 c-block2-title">HOW TO REGISTER</h3>
                            <div class="c-special-box-desc"><p>If you have already an account on the ISO Global Directory, you could access directly the digital learning platform using that account after accepting the related terms and conditions. Otherwise, you need to contact the <a href="#">ISO member in your country</a> to create you an account before completing the registration process.&nbsp;<br><br>For more information, please contact    <a href="mailto:Khammash@iso.org">Khammash@iso.org</a> &nbsp;<br></p></div>


                        </div>
                    </div>

                </div>-->

                <!-- Title Elearning courses -->
                <!--<div class="row">
                    <div class="col-md-12">
                        <h2 class="pl-5 font-weight-bold">ELEARNING COURSES</h2>
                    </div>
                </div>-->

                <!-- ./Title -->

                <!-- Elearning courses -->
                <div class="row pl-4 py-3" style="background-color: #f7f7f7">
                    <!--<div class="col-md-3 mx-5">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class="c-course-content row no-gutters"><a href="https://learning.iso.org/course/view.php?id=2" class="col-12 align-self-start p-0">
                                        <div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div></a><div class="c-course-box-content w-100"><h4 class="course-box-title"><span class="course-box-title-txt" title="Project Management in the ISO environment"><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span>-->
                    <!--<div class="course-box-icons">
                        <ul class="course-box-icons-list">
                            <li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i>
                            </li></ul></div>-->
                    <!--</h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>:&nbsp;English<br></p><p><b style="font-size:.85rem;">Delivery method :&nbsp;</b><span style="font-size:.85rem;">Self-paced course</span><br></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
</div>-->

                    <?php //foreach (getCategories('elearning') as $key => $course): ?>
                        <!--<div class="col-md-3 mx-5">
                            <div class="card-deck m-0">
                                <div class="card c-course-box" data-courseid="2" data-type="1">
                                    <div class="c-course-content row no-gutters">-->
                                        <!--<a href="https://learning.iso.org/course/view.php?id=<?= (int) $course['id'] ?>" class="col-12 align-self-start p-0">-->
                                            <!--<div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div>-->
                                            <!--<img src="<?= $_SERVER['REMOTE_ADDR'].getUrlOverviewFiles((int) $course['id']) ?>>" alt="" class="card-img-top">
                                        </a>
                                        <div class="c-course-box-content w-100">
                                            <h4 class="course-box-title">
                                        <span class="course-box-title-txt" title="Project Management in the ISO environment">
                                            <a class="" href="https://learning.iso.org/course/view.php?id=<?= (int) $course['id'] ?>"><?= $course['fullname'] ?></a>
                                        </span>
                                                <div class="course-box-icons"><ul class="course-box-icons-list"><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><?= $course['summary'] ?></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php //endforeach; ?>
                </div>-->
                <!-- ./ Elearning courses -->


                <!--<div class="row mt-3">
                    <div class="col-md-12">
                        <h2 class="pl-5 font-weight-bold">MICROLEARNING VIDEOS</h2>
                    </div>
                </div>-->


                <!-- Elearning miccrovideos -->
                <div class="row pl-2 py-3" style="background-color: #f7f7f7">
                    <!--<div class="col-md-3 mx-5 pt-5">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class="c-course-content row no-gutters"><a href="https://learning.iso.org/course/view.php?id=2" class="col-12 align-self-start p-0">
                                        <div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div></a><div class="c-course-box-content w-100"><h4 class="course-box-title"><span class="course-box-title-txt" title="Project Management in the ISO environment"><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class="course-box-icons"><ul class="course-box-icons-list"><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>:&nbsp;English<br></p><p><b style="font-size:.85rem;">Delivery method :&nbsp;</b><span style="font-size:.85rem;">Self-paced course</span><br></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
                    </div>-->

                    <?php //foreach ($microvideosCategories as $key => $mc):  ?>
                        <!--<div class="col-md-3 mx-5 pt-5">
                            <div class="card-deck m-0">
                                <div class="card c-course-box" data-courseid="2" data-type="1">
                                    <div class="c-course-content row no-gutters">
                                        <a href="https://learning.iso.org/course/view.php?id=<?= $mc['id'] ?>" class="col-12 align-self-start p-0">
                                            <div class="card-img-top myoverviewimg courseimage"        ></div>
                                        </a>
                                        <div class="c-course-box-content w-100">
                                            <h4 class="course-box-title">
                                                <span class="course-box-title-txt" title="<?= $mc['fullname'] ?>>"><a class="" href="https://learning.iso.org/course/view.php?id=<?= $mc['id'] ?>"><?= $mc['fullname'] ?></a></span>
                                                <div class="course-box-icons">
                                                    <ul class="course-box-icons-list">
                                                        <li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li>
                                                    </ul>
                                                </div>
                                            </h4>
                                            <div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
                                                <div class="os-resize-observer-host observed">
                                                    <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
                                                </div>
                                                <div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;">
                                                    <div class="os-resize-observer"></div></div>
                                                <div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div>
                                                <div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;">
                                                        <div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;"><?= $mc['summary'] ?></span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb"> <?= $mc['summary'] ?> </span></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
                        </div>-->
                    <?php //endforeach; ?>

                    <!--<div class="col-md-3 mx-5 pt-5">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class="c-course-content row no-gutters"><a href="https://learning.iso.org/course/view.php?id=2" class="col-12 align-self-start p-0"><div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div></a><div class="c-course-box-content w-100"><h4 class="course-box-title"><span class="course-box-title-txt" title="Project Management in the ISO environment"><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class="course-box-icons"><ul class="course-box-icons-list"><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>:&nbsp;English<br></p><p><b style="font-size:.85rem;">Delivery method :&nbsp;</b><span style="font-size:.85rem;">Self-paced course</span><br></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
                    </div>-->

                    <!--<div class="col-md-3 mx-5 pt-5">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class="c-course-content row no-gutters"><a href="https://learning.iso.org/course/view.php?id=2" class="col-12 align-self-start p-0"><div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div></a><div class="c-course-box-content w-100"><h4 class="course-box-title"><span class="course-box-title-txt" title="Project Management in the ISO environment"><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class="course-box-icons"><ul class="course-box-icons-list"><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>:&nbsp;English<br></p><p><b style="font-size:.85rem;">Delivery method :&nbsp;</b><span style="font-size:.85rem;">Self-paced course</span><br></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
                    </div>-->

                    <!--<div class="col-md-3 mx-5 pt-5">
                        <div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class="c-course-content row no-gutters"><a href="https://learning.iso.org/course/view.php?id=2" class="col-12 align-self-start p-0"><div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div></a><div class="c-course-box-content w-100"><h4 class="course-box-title"><span class="course-box-title-txt" title="Project Management in the ISO environment"><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class="course-box-icons"><ul class="course-box-icons-list"><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 160px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>:&nbsp;English<br></p><p><b style="font-size:.85rem;">Delivery method :&nbsp;</b><span style="font-size:.85rem;">Self-paced course</span><br></p><br><p></p><br><p></p></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div>
                    </div>-->
                </div>
                <!-- ./ Elearning microvideos -->


                <!--<div class="row no-gutters" style="border: 1px solid red">-->
                <!--<div class="logo">
                    <img src="https://learning.iso.org/pluginfile.php/1/core_admin/logo/0x300/1601276563/Logo.png" alt="ISO Learning" />
                </div>-->
                <!--</div>-->

                <!--<div id="settingsMenu" class="settings-menu" style="border: 1px solid yellow">
                    <div id="headerBtn" class="header-settings-menu-box">

                    </div>

                </div>-->

            </div>


            <!--<div id="block2" class="s-block-2 l-container s-special-box section-padding row no-gutters justify-content-sm-center justify-content-lg-between">

                <div class="col-12 row no-gutters justify-content-between py-0 px-1">
                    <div id="block2-1" class=" col-sm-12 col-md-6 my-5 p-sm-3 p-md-5">



                        <h3 class="h3 c-block2-title">LEARN ONLINE WITH ISO</h3>
                        <div class="c-special-box-desc"><p>The ISO digital learning platform offers a wide range of digital learning solutions in an effort to provide the learners with a great deal of flexibility, allowing them to study at any time from any place at their own convenient speed without worrying about timetables and schedules.&nbsp;<br>Unlike traditional classroom learning opportunities, online learning environments foster additional learning experiences where learners can interect, collaborate and retain information in a far better manner.&nbsp;<br></p></div>


                    </div>
                    <div id="block2-2" class=" col-sm-12 col-md-6 my-5 p-sm-3 p-md-5">



                        <h3 class="h3 c-block2-title">HOW TO REGISTER</h3>
                        <div class="c-special-box-desc"><p>If you have already an account on the ISO Global Directory, you could access directly the digital learning platform using that account after accepting the related terms and conditions. Otherwise, you need to contact the ISO member in your country to create you an account before completing the registration process.&nbsp;<br><br>For more information, please contact Khammash@iso.org&nbsp;<br></p></div>


                    </div>
                </div>-->



        </div>

        <hr class="hr my-0" />


        <!-- C'est dans cette partie que nous avons le plus modifié il appartien plus a la version initial du site -->
        <!-- <div class="l-block-8 l-container mt-5">
            <div id="page-content" class="s-block-8 row no-gutters page-main-content">
                <div id="region-main-box">
                    <section id="region-main" >

                     suppression de la section ISO Digital Learning -->
                        <!-- <section data-region="blocks-main-top-widget" class="blocks-main-top-widget d-print-none">
                            <aside id="block-region-maintopwidgets" class="block-region" data-blockregion="maintopwidgets" data-droptarget="1">
                                <section id="inst41"
                                                                                                                                                        class=" block block_html  card mb-4"
                                                                                                                                                        role="complementary"
                                                                                                                                                        data-block="html"
                                                                                                                                                        aria-label="HTML"
                                    >

                                    <div class="card-body">



                                        <div class="card-text content mt-3">
                                         suppression du petit titre 
                                             <div class="no-overflow"><h3 style="text-align: center;"><b style="color: inherit; font-family: inherit; font-size: 1.35rem;">ISO Digital Learning Platform</b><br></h3></div> 
                                            <div class="footer"></div>

                                        </div>

                                    </div> 

                                </section>
                            </aside>
                        </section> 
                        <span class="notifications" id="user-notifications"></span>
                        <div role="main"><span id="maincontent"></span><div class="box py-3 mdl-align card-content"><form action="https://learning.iso.org/course/search.php" id="coursesearch" method="get" class="form-inline">
                                    <fieldset class="coursesearchbox invisiblefieldset m-b-2">
                                     Suppresion du label de recherche -->
                                        <!-- <label for="shortsearchbox">Search courses</label> -->
                                        <!-- Suppression de la bare de recherche-->
                                        <!-- <input id="shortsearchbox" name="search" type="text" size="45" value="" class="m-sm-l-0 m-md-l-1 my-sm-1 my-md-0 form-control"> -->
                                        <!-- button recherche elevé -->
                                        <!-- <button class="btn btn-secondary" type="submit">Go</button> 
                                    </fieldset>
                                     enlevement du available courses 
                                </form></div><br /><a class="skip-block skip" href="#skipavailablecourses">Skip available courses</a><div id="frontpage-available-course-list"><h2>Available courses</h2><div class="s-courses frontpage-course-list-all"><div class="card-deck m-0"><div class="card c-course-box" data-courseid="2" data-type="1"><div class='c-course-content row no-gutters'><a href='https://learning.iso.org/course/view.php?id=2' class='col-12 align-self-start p-0'><div class='card-img-top myoverviewimg courseimage' style='background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);'></div></a><div class="c-course-box-content w-100"><h4 class='course-box-title'><span class='course-box-title-txt' title='Project Management in the ISO environment'><a class="" href="https://learning.iso.org/course/view.php?id=2">Project Management in the ISO environment</a></span><div class='course-box-icons'><ul class='course-box-icons-list'><li><i class="icon fas fa-sign-in-alt fa-fw " aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul></div></h4><div class='course-box-content-desc'><div class="course-box-desc"><div class="no-overflow"><p></p><p><span style="font-size:.85rem;">This e-learning course you the Committee Manager, and support team, to successfully deliver high quality documents in a timely manner. It helps everyone in the ISO Community be effective and efficient when the availability of resources is a challenge:</span><br /></p><p><span lang="en-us" xml:lang="en-us"></span></p><ul type="disc"><li><span lang="en-gb" xml:lang="en-gb">Planning: Identifying what matters when scheduling the project development, and when setting realistic target dates for your projects</span></li><li><span lang="en-gb" xml:lang="en-gb">Monitoring: Understanding the tools and solutions available to you, and defining your own strategy in terms of pro-activity</span></li><li><span lang="en-gb" xml:lang="en-gb">Working together: Understanding and providing the appropriate support to the key roles in project development (Convenors and project leaders)</span></li></ul><p></p><p><b>Language </b>: English<br /></p><p><b style="font-size:.85rem;">Delivery method : </b><span style="font-size:.85rem;">Self-paced course</span><br /></p><br /><p></p><br /><p></p></div></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=2">Get access</a></div></div></div></div></div></div></div><span class="skip-block-to" id="skipavailablecourses"></span><br /></div> 


                    </section>


                </div>
            </div>
        </div> -->


        <hr class="hr my-0" />



        <!--<div id="block11" class="s-block-11 l-container s-special-box section-padding row no-gutters justify-content-sm-center justify-content-lg-between">


            <div class="courses-slider  c-course-container m-t-5 px-0 py-0">

                <div class="c-course-slider-item mb-5">
                    <div class=" c-course-card-item">
                        <div class="c-course-card" id="block11-1">


                            <div class="c-course-card-content">

                            </div>

                        </div>
                    </div>
                </div>


                <div class="c-course-slider-item mb-5">
                    <div class=" c-course-card-item">
                        <div class="c-course-card" id="block11-2">


                            <div class="c-course-card-content">

                            </div>

                        </div>
                    </div>
                </div>


                <div class="c-course-slider-item mb-5">
                    <div class=" c-course-card-item">
                        <div class="c-course-card" id="block11-3">


                            <div class="c-course-card-content">

                            </div>

                        </div>
                    </div>
                </div>


                <div class="c-course-slider-item mb-5">
                    <div class=" c-course-card-item">
                        <div class="c-course-card" id="block11-4">


                            <div class="c-course-card-content">

                            </div>

                        </div>
                    </div>
                </div>



            </div>< Courses list end -->



        <!--</div>-->
        <hr class="hr my-0" />

        <script>
            var slider = tns({
                container: '.courses-slider',
                items: 1,
                slideBy: 1,
                mouseDrag: true,
                controlsText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                loop: false,
                controls: true,
                responsive: {
                    768: {
                        items: 2,
                        slideBy: 2,
                    },
                    1200: {
                        items: 4,
                        slideBy: 4,
                    }
                }
            });
        </script>



        <!--<footer id="page-footer" class="s-footer">

            <div class="c-container">



                <div class="s-footer--copy row no-gutters">

                    <div class="s-footer--additional col-12 contact p-0">
                        <ul class="list-inline">



                        </ul>
                    </div>


                    <div class="col-12 my-3 p-0">
                        <span class="copy">© All Rights Reserved</span></br><span class="sep">Great Thinks Happen when the word agrees</span>
                    </div>


                </div>

                <div class="o-course-footer">
                    <div id="course-footer"></div>
                    <div class="tool_usertours-resettourcontainer"></div>
                </div>

                <div class="o-standard-footer-html"><div class="policiesfooter"><a href="https://learning.iso.org/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Flearning.iso.org%2F">Policies</a></div></div>

            </div>

        </footer>-->

        <a href="#top" class="back-to-top" id="backToTop" title="Back to top"><i class="fas fa-arrow-circle-up"></i></a>




        <script>
            //<![CDATA[
            var require = {
                baseUrl : 'https://learning.iso.org/lib/requirejs.php/1601276563/',
                // We only support AMD modules with an explicit define() statement.
                enforceDefine: true,
                skipDataMain: true,
                waitSeconds : 0,

                paths: {
                    jquery: 'https://learning.iso.org/lib/javascript.php/1601276563/lib/jquery/jquery-3.4.1.min',
                    jqueryui: 'https://learning.iso.org/lib/javascript.php/1601276563/lib/jquery/ui-1.12.1/jquery-ui.min',
                    jqueryprivate: 'https://learning.iso.org/lib/javascript.php/1601276563/lib/requirejs/jquery-private'
                },

                // Custom jquery config map.
                map: {
                    // '*' means all modules will get 'jqueryprivate'
                    // for their 'jquery' dependency.
                    '*': { jquery: 'jqueryprivate' },
                    // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
                    '*': { process: 'core/first' },

                    // 'jquery-private' wants the real jQuery module
                    // though. If this line was not here, there would
                    // be an unresolvable cyclic dependency.
                    jqueryprivate: { jquery: 'jquery' }
                }
            };

            //]]>
        </script>
        <script src="https://learning.iso.org/lib/javascript.php/1601276563/lib/requirejs/require.min.js"></script>
        <script>
            //<![CDATA[
            M.util.js_pending("core/first");require(['core/first'], function() {
                ;
                require(["media_videojs/loader"], function(loader) {
                    loader.setUp(function(videojs) {
                        videojs.options.flash.swf = "https://learning.iso.org/media/player/videojs/videojs/video-js.swf";
                        videojs.addLanguage('en', {
                            "Audio Player": "Audio Player",
                            "Video Player": "Video Player",
                            "Play": "Play",
                            "Pause": "Pause",
                            "Replay": "Replay",
                            "Current Time": "Current Time",
                            "Duration": "Duration",
                            "Remaining Time": "Remaining Time",
                            "Stream Type": "Stream Type",
                            "LIVE": "LIVE",
                            "Seek to live, currently behind live": "Seek to live, currently behind live",
                            "Seek to live, currently playing live": "Seek to live, currently playing live",
                            "Loaded": "Loaded",
                            "Progress": "Progress",
                            "Progress Bar": "Progress Bar",
                            "progress bar timing: currentTime={1} duration={2}": "{1} of {2}",
                            "Fullscreen": "Fullscreen",
                            "Non-Fullscreen": "Non-Fullscreen",
                            "Mute": "Mute",
                            "Unmute": "Unmute",
                            "Playback Rate": "Playback Rate",
                            "Subtitles": "Subtitles",
                            "subtitles off": "subtitles off",
                            "Captions": "Captions",
                            "captions off": "captions off",
                            "Chapters": "Chapters",
                            "Descriptions": "Descriptions",
                            "descriptions off": "descriptions off",
                            "Audio Track": "Audio Track",
                            "Volume Level": "Volume Level",
                            "You aborted the media playback": "You aborted the media playback",
                            "A network error caused the media download to fail part-way.": "A network error caused the media download to fail part-way.",
                            "The media could not be loaded, either because the server or network failed or because the format is not supported.": "The media could not be loaded, either because the server or network failed or because the format is not supported.",
                            "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.",
                            "No compatible source was found for this media.": "No compatible source was found for this media.",
                            "The media is encrypted and we do not have the keys to decrypt it.": "The media is encrypted and we do not have the keys to decrypt it.",
                            "Play Video": "Play Video",
                            "Close": "Close",
                            "Close Modal Dialog": "Close Modal Dialog",
                            "Modal Window": "Modal Window",
                            "This is a modal window": "This is a modal window",
                            "This modal can be closed by pressing the Escape key or activating the close button.": "This modal can be closed by pressing the Escape key or activating the close button.",
                            ", opens captions settings dialog": ", opens captions settings dialog",
                            ", opens subtitles settings dialog": ", opens subtitles settings dialog",
                            ", opens descriptions settings dialog": ", opens descriptions settings dialog",
                            ", selected": ", selected",
                            "captions settings": "captions settings",
                            "subtitles settings": "subititles settings",
                            "descriptions settings": "descriptions settings",
                            "Text": "Text",
                            "White": "White",
                            "Black": "Black",
                            "Red": "Red",
                            "Green": "Green",
                            "Blue": "Blue",
                            "Yellow": "Yellow",
                            "Magenta": "Magenta",
                            "Cyan": "Cyan",
                            "Background": "Background",
                            "Window": "Window",
                            "Transparent": "Transparent",
                            "Semi-Transparent": "Semi-Transparent",
                            "Opaque": "Opaque",
                            "Font Size": "Font Size",
                            "Text Edge Style": "Text Edge Style",
                            "None": "None",
                            "Raised": "Raised",
                            "Depressed": "Depressed",
                            "Uniform": "Uniform",
                            "Dropshadow": "Dropshadow",
                            "Font Family": "Font Family",
                            "Proportional Sans-Serif": "Proportional Sans-Serif",
                            "Monospace Sans-Serif": "Monospace Sans-Serif",
                            "Proportional Serif": "Proportional Serif",
                            "Monospace Serif": "Monospace Serif",
                            "Casual": "Casual",
                            "Script": "Script",
                            "Small Caps": "Small Caps",
                            "Reset": "Reset",
                            "restore all settings to the default values": "restore all settings to the default values",
                            "Done": "Done",
                            "Caption Settings Dialog": "Caption Settings Dialog",
                            "Beginning of dialog window. Escape will cancel and close the window.": "Beginning of dialog window. Escape will cancel and close the window.",
                            "End of dialog window.": "End of dialog window.",
                            "{1} is loading.": "{1} is loading."
                        });

                    });
                });;


                require(['jquery', 'tool_policy/jquery-eu-cookie-law-popup', 'tool_policy/policyactions'], function($, Popup, ActionsMod) {
                    // Initialise the guest popup.
                    $(document).ready(function() {
                        // Only show message if there is some policy related to guests.
                        // Get localised messages.
                        var textmessage = "If you continue browsing this website, you agree to our policies:" +
                            "<ul>" +
                            "<li>" +
                            "<a href=\"https://learning.iso.org/admin/tool/policy/view.php?versionid=4&amp;returnurl=https%3A%2F%2Flearning.iso.org%2F\" " +
                            "   data-action=\"view-guest\" data-versionid=\"4\" data-behalfid=\"1\" >" +
                            "ISO Learning Terms" +
                            "</a>" +
                            "</li>" +
                            "" +
                            "</ul>";
                        var continuemessage = "Continue";

                        // Initialize popup.
                        $(document.body).addClass('eupopup');
                        if ($(".eupopup").length > 0) {
                            $(document).euCookieLawPopup().init({
                                popupPosition: 'bottom',
                                popupTitle: '',
                                popupText: textmessage,
                                buttonContinueTitle: continuemessage,
                                buttonLearnmoreTitle: '',
                                compactStyle: true,
                            });
                        }

                        // Initialise the JS for the modal window which displays the policy versions.
                        ActionsMod.init('[data-action="view-guest"]');
                    });
                });

                ;

                require(['jquery'], function($) {

                    var searchbar = $("#searchField"),
                        searchbox = $(".search-box"),
                        searchbarToggle = $("#searchBtn"),
                        searchbarClose = $("#searchBtnClose");

                    searchbarToggle.on("click", function(e) {
                        e.preventDefault();
                        searchbar.toggleClass("show");
                        searchbox.toggleClass("active");
                    });

                    searchbarClose.on("click", function(e) {
                        searchbar.removeClass("show");
                        searchbox.removeClass("active");
                    });

                    var mobileTopBar = $(".s-top-container"),
                        mobileTopBarToggle = $("#mobileTopBarBtn, #mobileTopBarCloseBtn");
                    mobileSidebarToggle = $(" #mobileSidebarBtnClose, #mobileSidebarBtn");

                    mobileTopBarToggle.on("click", function(e) {
                        e.preventDefault();
                        mobileTopBar.toggleClass("show");
                        mobileTopBarToggle.toggleClass("hidden");
                    });

                    var sidebar = $("#nav-drawer"),
                        sidebarToggle = $(".sidebar-btn, .sidebar-btn--close");
                    pageWrapper = $("#page-wrapper, #page-footer .c-container");
                    topBarWrapper = $(".s-top");

                    mobileSidebarToggle.on("click", function(e) {
                        e.preventDefault();
                        mobileSidebarToggle.toggleClass("hidden");
                    });

                    /*
                     * Bootstrap Cookie Alert by Wruczek
                     * https://github.com/Wruczek/Bootstrap-Cookie-Alert
                     * Released under MIT license
                     */
                    (function () {
                        "use strict";

                        var customAlert = document.querySelector("#customAlert");
                        var closeCustomAlert = document.querySelector("#customAlertClose");

                        if (!customAlert) {
                            return;
                        }

                        customAlert.offsetHeight; // Force browser to trigger reflow (https://stackoverflow.com/a/39451131)

                        // Show the alert if we cant find the "closeCustomAlert" cookie
                        if (!getCookie("closeCustomAlert")) {
                            customAlert.classList.add("show");
                        }

                        // When clicking on the agree button, create a 1 year
                        // cookie to remember user's choice and close the banner
                        closeCustomAlert.addEventListener("click", function () {
                            setCookie("closeCustomAlert", true, 1);
                            customAlert.classList.remove("show");
                        });

                        // Cookie functions from w3schools
                        function setCookie(cname, cvalue, exdays) {
                            var d = new Date();
                            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                            var expires = "expires=" + d.toUTCString();
                            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                        }

                        function getCookie(cname) {
                            var name = cname + "=";
                            var decodedCookie = decodeURIComponent(document.cookie);
                            var ca = decodedCookie.split(';');
                            for (var i = 0; i < ca.length; i++) {
                                var c = ca[i];
                                while (c.charAt(0) === ' ') {
                                    c = c.substring(1);
                                }
                                if (c.indexOf(name) === 0) {
                                    return c.substring(name.length, c.length);
                                }
                            }
                            return "";
                        }
                    })();


                    //Back to top button
                    var btn = $('#backToTop');

                    $(window).scroll(function() {
                        if ($(window).scrollTop() > 300) {
                            btn.addClass('show');
                        } else {
                            btn.removeClass('show');
                        }
                    });

                    btn.on('click', function(e) {
                        e.preventDefault();
                        $('html, body').animate({scrollTop:0}, '300');
                    });


                });
                ;

                require(['theme_space/loader']);
                ;
                M.util.js_pending('core/notification'); require(['core/notification'], function(amd) {amd.init(2, []); M.util.js_complete('core/notification');});;
                M.util.js_pending('core/log'); require(['core/log'], function(amd) {amd.setConfig({"level":"warn"}); M.util.js_complete('core/log');});;
                M.util.js_pending('core/page_global'); require(['core/page_global'], function(amd) {amd.init(); M.util.js_complete('core/page_global');});M.util.js_complete("core/first");
            });
            //]]>
        </script>
        <script>
            //<![CDATA[
            M.str = {"moodle":{"lastmodified":"Last modified","name":"Name","error":"Error","info":"Information","yes":"Yes","no":"No","cancel":"Cancel","confirm":"Confirm","areyousure":"Are you sure?","closebuttontitle":"Close","unknownerror":"Unknown error","file":"File","url":"URL"},"repository":{"type":"Type","size":"Size","invalidjson":"Invalid JSON string","nofilesattached":"No files attached","filepicker":"File picker","logout":"Logout","nofilesavailable":"No files available","norepositoriesavailable":"Sorry, none of your current repositories can return files in the required format.","fileexistsdialogheader":"File exists","fileexistsdialog_editor":"A file with that name has already been attached to the text you are editing.","fileexistsdialog_filemanager":"A file with that name has already been attached","renameto":"Rename to \"{$a}\"","referencesexist":"There are {$a} alias\/shortcut files that use this file as their source","select":"Select"},"admin":{"confirmdeletecomments":"You are about to delete comments, are you sure?","confirmation":"Confirmation"},"debug":{"debuginfo":"Debug info","line":"Line","stacktrace":"Stack trace"},"langconfig":{"labelsep":": "}};
            //]]>
        </script>
        <script>
            //<![CDATA[
            (function() {Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.configure({"mathjaxconfig":"\nMathJax.Hub.Config({\n    config: [\"Accessible.js\", \"Safe.js\"],\n    errorSettings: { message: [\"!\"] },\n    skipStartupTypeset: true,\n    messageStyle: \"none\"\n});\n","lang":"en"});
            });
                M.util.help_popups.setup(Y);
                M.util.js_pending('random5f7edf36734a92'); Y.on('domready', function() { M.util.js_complete("init");  M.util.js_complete('random5f7edf36734a92'); });
            })();
            //]]>
        </script>


        <script>
            document.addEventListener("DOMContentLoaded", function() {
                //The first argument are the elements to which the plugin shall be initialized
                //The second argument has to be at least a empty object or a object with your desired options
                OverlayScrollbars(document.querySelectorAll('#nav-drawer, .conversation-cotnent-container'),
                    {scrollbars :
                    {
                        visibility       : "auto",
                        autoHide         : "l",
                        autoHideDelay    : 800,
                        dragScrolling    : true,
                        clickScrolling   : false,
                        touchSupport     : true
                    }
                    });
                OverlayScrollbars(document.querySelectorAll('.region-main-settings-menu .dropdown-menu'),
                    {scrollbars :
                    {
                        visibility       : "auto",
                        autoHide         : "never",
                        autoHideDelay    : 800,
                        dragScrolling    : true,
                        clickScrolling   : false,
                        touchSupport     : true
                    }
                    });
                OverlayScrollbars(document.querySelectorAll('.s-courses .course-box-content-desc, .popover-region-content-container, .form-description pre, .accordion-nav-content'),
                    {scrollbars :
                    {
                        visibility       : "auto",
                        dragScrolling    : true,
                        clickScrolling   : false,
                        touchSupport     : true
                    }
                    });
            });
        </script>

        <script>
            var navbar = document.getElementById("navBar");
            var sticky = navbar.offsetTop;
            var stickysettingbtn = document.getElementById("settingsMenu");

            function stickyNavF() {
                if (window.pageYOffset >= sticky) {
                    navbar.classList.add("sticky-nav");
                    if(stickysettingbtn != null) { stickysettingbtn.classList.add("sticky"); }
                } else {
                    navbar.classList.remove("sticky-nav");
                    if(stickysettingbtn != null) { stickysettingbtn.classList.remove("sticky"); }
                }
            }
            window.onscroll = function() {stickyNavF()};
        </script>


        <script src="https://learning.iso.org/theme/space/addons/bootstrapnavbar/bootstrap-4-navbar.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<footer id="page-footer" class="s-footer">

    <div class="c-container">



        <div class="s-footer--copy row no-gutters">

            <div class="s-footer--additional col-12 contact p-0">
                <ul class="list-inline">



                </ul>
            </div>


            <div class="col-12 my-3 p-0">
                <span class="copy">© All Rights Reserved</span><br><span class="sep">"Change is the end result of all true learning (Leo Buscaglia)"</span>
            </div>


        </div>

        <div class="o-course-footer">
            <div id="course-footer"></div>
            <div class="tool_usertours-resettourcontainer"></div>
        </div>

        <div class="o-standard-footer-html"><div class="policiesfooter"><a href="https://learning.iso.org/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Flearning.iso.org%2F">Policies</a></div></div>

    </div>

</footer>
</body>
</html>