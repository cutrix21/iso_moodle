<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Apercu mail</title>
</head>
<body>
	<h4>Dear Participant, </h4>
	<p>Thank you very much for completing/watching the eLearning course/microlearning video through the ISO digital learning platform.</p>
<p>ISO puts great emphasis on improving continually its digital training activities. Therefore, we would be grateful if could complete the satisfaction survey that will help us to check whether or not we have been able to meet your learning and development needs. It takes 5 minutes to complete it.</p>
<p>Please click on <b>one of the links</b> listed below to access the survey and complete it in less than 5 mins.</p>
<p>
	<ul>
		<li><a href="https://www.smartsurvey.co.uk/s/eLearningPM/">https://www.smartsurvey.co.uk/s/eLearningPM/</a> (only for the ISO eLearning course on Project Management for Committee Managers that have you just completed).</li>
		<li><a href="https://www.smartsurvey.co.uk/s/third-partycourses/">	https://www.smartsurvey.co.uk/s/third-partycourses/</a> (only for the third-party eLearning course or ISO microlearning course that have you just completed). </li>
	</ul>
</p>
<p>
	Alternatively, you can copy and paste one of the above-listed links into your internet browser and complete the survey.
</p>
<p>
	Should you have any questions about the survey or in case you encounter any difficulty, please contact us at <a href="mailto:capacity@iso.org">capacity@iso.org</a>
</p>
<p>Thank you again for your participation and we look forward to your valuable feedback.</p>
<p><b>Best regards</b></p>
<p><b>ISO capacity building team </b></p>
</body>
</html>