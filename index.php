<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*require 'config.php';
require 'lib/completionlib.php';*/
//require_once("{$CFG->libdir}/completionlib.php");

/*function ToObject($Array) {

    // Create new stdClass object
    $object = new stdClass();

    // Use loop to convert array into
    // stdClass object
    foreach ($Array as $key => $value) {
        if (is_array($value)) {
            $value = ToObject($value);
        }
        $object->$key = $value;
    }
    return $object;
}

try {
    global $USER;
    $userid = $USER->id;
    //var_dump($userid);
    //var_dump(enrol_get_my_courses(['id', 'shortname', 'fullname']));
    $c = enrol_get_my_courses(['id', 'shortname', 'fullname']);
    $course_object = new stdClass();
    $course_object = $c;

    var_dump($course_object);

    //$cinfo = new completion_info($course_object);
    die;



} catch (coding_exception $e) {
    echo $e->getMessage();
}
*/
//require 'lib/completionlib.php';

/*function check_enrol($shortname, $userid, $roleid, $enrolmethod = 'manual') {
    global $DB;
    $user = $DB->get_record('user', array('id' => $userid, 'deleted' => 0), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('shortname' => $shortname), '*', MUST_EXIST);
    $context = context_course::instance($course->id);
    if (!is_enrolled($context, $user)) {
        $enrol = enrol_get_plugin($enrolmethod);
        if ($enrol === null) {
            return false;
        }
        $instances = enrol_get_instances($course->id, true);
        $manualinstance = null;
        foreach ($instances as $instance) {
            if ($instance->name == $enrolmethod) {
                $manualinstance = $instance;
                break;
            }
        }
        if ($manualinstance !== null) {
            $instanceid = $enrol->add_default_instance($course);
            if ($instanceid === null) {
                $instanceid = $enrol->add_instance($course);
            }
            $instance = $DB->get_record('enrol', array('id' => $instanceid));
        }
        $enrol->enrol_user($instance, $userid, $roleid);
    }
    return true;
}*/

require_once 'database.php';

const BASE_URL_LOCAL = "http://localhost/iso_moodle";
const BASE_URL_REMOTE = "https://learning.iso.org";


$db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);


function getUrlOverviewFiles($idcourse)
{
    global $db;
    $query = $db->query("select cont.instanceid idCourse, f.filename filename, cont.id contexte
from mdl_context cont
inner join mdl_files f
on f.contextid = cont.id
where cont.instanceid = '" . $idcourse . "'
and f.component = 'course'
and f.filename <> '.'
order by f.id desc");
//$query->execute(array((int) $idcourse));
    $data = $query->fetch();
    return "/pluginfile.php/" . $data['contexte'] . "/course/overviewfiles/" . $data['filename'];
}


function getCategories($libCategorie)
{
    global $db;
    $query = $db->query("SELECT * FROM mdl_course WHERE category = (SELECT id FROM mdl_course_categories WHERE name = '" . $libCategorie . "')");
    return $query->fetchAll();
}


function image_resize($file_name, $width, $height, $crop = FALSE)
{
    list($wid, $ht) = getimagesize($file_name);
    $r = $wid / $ht;
    if ($crop) {
        if ($wid > $ht) {
            $wid = ceil($wid - ($width * abs($r - $width / $height)));
        } else {
            $ht = ceil($ht - ($ht * abs($r - $wid / $ht)));
        }
        $new_width = $width;
        $new_height = $height;
    } else {
        if ($width / $height > $r) {
            $new_width = $height * $r;
            $new_height = $height;
        } else {
            $new_height = $width / $r;
            $new_width = $width;
        }
    }
    $source = imagecreatefromjpeg($file_name);
    $dst = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($dst, $source, 0, 0, 0, 0, $new_width, $new_height, $wid, $ht);
    return $dst;
}

/*
$url = "https://learning.iso.org/pluginfile.php/435/course/overviewfiles/tz7n-7vqceaq86dprdnzag.jpg";
$img = 'test.jpeg';
$path = file_put_contents($img, file_get_contents($url));

//var_dump($path);

$img_to_resize = image_resize($img, 600, 350, true);
/*
imagejpeg($img_to_resize, 'test_resize.jpeg');
echo "<img src='test_resize.jpeg'>";
die;*/

function getResizedImg($url)
{
    /*if (!file_exists("resize_img/")) {
        mkdir("resize_img/", 0755);
    }*/

    $name = "resize_img/" . explode("/", $url)[7];
    //chmod("resize_img/", 0755);

    //Download image
    $resizeLocalImg = glob("resize_img/*");
    foreach ($resizeLocalImg as $img) {
        if ($name === $img) {
            $customLinkImg = $name;
            return $customLinkImg;
        }
    }
    file_put_contents($name, file_get_contents($url));

    $img_to_resize = image_resize($name, 600, 350, true);

    imagejpeg($img_to_resize, $name);

    $customLinkImg = "https://learning.iso.org/resize_img/" . $name;
    //$customLinkImg = "http://localhost/resize_img/".$name;

    return $customLinkImg;
}


$elearningCaegories = getCategories('elearning');
$microvideosCategories = getCategories('microlearning');
$defaultUrl = "https://itsocial.fr/wp-content/uploads/2018/05/iStock-869155894.png";

$imageTestArray = [
    //"http://localhost:8888/iso_moodle/photos/img_nana.jpeg",
    "http://localhost:8888/iso_moodle/resize_img/img_grand.jpeg",
    //"http://localhost:8888/iso_moodle/photos/img_grand.jpeg",
    //"https://learning.iso.org/pluginfile.php/435/course/overviewfiles/tz7n-7vqceaq86dprdnzag.jpg",
    "http://localhost:8888/iso_moodle/resize_img/test_resize.jpeg",
    "https://learning.iso.org/pluginfile.php/439/course/overviewfiles/cours.jpg",
    "https://learning.iso.org/pluginfile.php/440/course/overviewfiles/cours.jpg%3E"
];
$placeholderArray = [
    "https://learning.iso.org/pluginfile.php/435/course/overviewfiles/tz7n-7vqceaq86dprdnzag.jpg",
    "http://localhost:8888/iso_moodle/resize_img/image_nana.jpeg",
    "http://localhost:8888/iso_moodle/resize_img/img_grand.jpeg",
    "http://localhost:8888/iso_moodle/resize_img/img_nana_presentation.jpg",
    "http://localhost:8888/iso_moodle/resize_img/img_cours.jpg",
    "https://via.placeholder.com/600x350.jpeg",
    "https://via.placeholder.com/600x350.jpeg",
    "https://via.placeholder.com/600x350.jpeg",
    "https://via.placeholder.com/600x350.jpeg",
    "https://via.placeholder.com/600x350.jpeg",
];
$imgIsoOnline = [
    "https://learning.iso.org/resize_img/01.jpg",
    "https://learning.iso.org/resize_img/conseiller-a-distance.jpg",
    "https://learning.iso.org/resize_img/building-blocksx600.jpg",
    "https://learning.iso.org/resize_img/participatingx600.jpg",
    "https://learning.iso.org/resize_img/draftingx600.jpg"
];
/*$query = $db->query("SELECT * FROM mdl_course WHERE category = 'elearning'");

$query2 = $db->query("SELECT * FROM mdl_course WHERE category = 'microlearning'");
var_dump($query2->fetchAll());*/


//var_dump(getCategories('elearning'));
/*var_dump(getUrlOverviewFiles(13));
die;*/

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISO - Digital learning platform</title>

    <!-- Bootstrap -->
    <!--<link href="css/bootstrap-4.4.1.css" rel="stylesheet">-->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.13.0/css/OverlayScrollbars.css">
    <!-- All -->
    <link href="custom.css" rel="stylesheet">
    <style>
        .course-box-desc ul li span {font-size:0.85rem!important}
    </style>
</head>
<body>
<!-- STRATE 01 -->
<div class="container">
    <div class="row text-right pt-3 pb-3">
        <div class="col-12">
            <a href="https://learning.iso.org/my" class="login-btn"
               style="background-color: #E60100 !important; border-radius: 0px; border: 0px solid transparent !important; font-family: sans-serif;">Log
                in
                <i class="fa fa-sign-in"></i>
            </a>
        </div>
    </div>
</div>
<!-- -->
<hr style="margin-top: 0rem; margin-bottom: 0rem;">
<!-- STRATE 02 -->
<div class="container">
    <div class="row text-left pt-4 pb-4">
        <div class="col-12">
            <div class="media" style="align-items: center;">
                <img class="align-self-center mr-4"
                     src="https://learning.iso.org/pluginfile.php/1/core_admin/logo/0x300/1601276563/Logo.png"
                     alt="ISO Learning" style="max-width:100px;">
                <div class="media-body">
                    <h1 class="mt-0" style="font-size: 60px; font-family: sans-serif;">DIGITAL LEARNING
                        PLATFORM</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- STRATE 03 -->

<div class="jumbotron jumbotron-fluid text-center" style="background-color: #f7f7f7; margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 align-top d-flex justify-content-center">
                <form style="display:inline; width: 100%" class="form-inline"
                      action="https://learning.iso.org/course/search.php" method="get">
                    <div class="align-self-center center-block">
                        <input type="search" name="search" placeholder="Search Learning Solutions"
                               class="align-top form-control"
                               style="display:inline; width:calc(99% - 50px); height:52px; border-radius: 0px;">
                        <button class="align-top btn btn-secondary" type="submit" id="go"
                                style="background-color: #E60100; border-radius: 0px; border: 0px solid transparent; font-family: sans-serif; display:inline; width:50px; height:51px;">
                            Go
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- STRATE 04 -->

<div class="container">
    <div class="row pt-4 pb-4">
        <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 pl-4">
            <h4 style="font-family: sans-serif;">LEARN ONLINE WITH ISO</h4>
            <p style="font-family: sans-serif">The ISO digital learning platform offers a wide range of
                digital learning solutions in an effort to
                provide the learners with a great deal of flexibility, allowing them to study at any time from any place
                at their own convenient speed without worrying about timetables and schedules.
                Unlike traditional classroom learning opportunities, online learning environments foster additional
                learning experiences where learners can interact, collaborate and retain information in a far better
                manner. </p>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 pl-4">
            <h4 style="font-family: sans-serif;">HOW TO REGISTER</h4>
            <p style="font-family: sans-serif">If you have already an account on the ISO Global
                Directory, you could access directly the digital
                learning platform using that account after accepting the related terms and conditions. Otherwise, you
                need to contact the <a href="https://www.iso.org/members.html">ISO member in your country</a> to create
                you an account before completing the
                registration process.
                <br/><br/>
                For more information, please contact <a href="mailto:khammash@iso.org">khammash@iso.org</a></p>
        </div>
    </div>
</div>
<!-- STRATE 05 -->
<div class="jumbotron jumbotron-fluid" style="background-color: #f7f7f7;margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="pb-4" style="font-family: sans-serif;">ISO ELEARNING COURSES</h4>
            </div>
        </div>

        <div class="row">
	    <?php foreach (getCategories("elearning") as $k => $c): ?>
              <?php if($c['visible']): ?>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="
    padding-left: 0;">
                    <div class="card-deck m-0 h-auto">
                        <div class="card c-course-box mb-5" data-courseid="2" data-type="1">
                            <div class="c-course-content row no-gutters p-0">
                                <div>
                                    <a href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                       class="col-12 align-self-start p-0">
                                        <!--<div class="course-box-icons-list position-absolute p-1 mt-2"
                                             style="right: 10%; background-color: rgba(0,0,0, .4)">
                                            <i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true"
                                               title="Self enrolment" aria-label="Self enrolment"></i>
                                        </div>-->
                                        <!--<div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div>-->
                                        <!-- <img src="<?php // getResizedImg($imgIsoOnline[$k]) ?>" class="img-fluid">-->
                                        <?php $url = "https://learning.iso.org" . getUrlOverviewFiles((int)$c['id']) ?>
                                        <img src="<?= getResizedImg($url) ?>" alt="" class="img-fluid">
                                    </a>
                                </div>

                                <div class="c-course-box-content w-100">
                                    <!-- Envevement de la scrollbar sur le titre du cours -->
                                    <!-- <div class="" style="height: 48px; overflow-x: hidden; overflow-y: scroll"> -->
                                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12"
                                         style="overflow-x: hidden; overflow-y: hidden; height: 80px">

                                        <h4 class="course-box-title font-weight-bold pt-3"
                                            style="font-family:  sans-serif;">
                                        <span class="course-box-title-txt px-0 pt-2" title="<?= $c['fullname'] ?>">
                                            <a class=""
                                               href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                               style="color: #000"><?= $c['fullname'] ?></a>
                                        </span>
                                            <!--<div class="course-box-icons" style="top: 3%; left: 80%">
                                                <ul class="course-box-icons-list">
                                                    <li><i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul>
                                            </div>-->
                                        </h4>
                                    </div>
                                    <!--<div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 75px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc p-0"><div class="no-overflow"><?= $mc['summary'] ?></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end m-0"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=<?= $mc['id'] ?>" style="border-radius: 0px">Get access</a>-->
                                </div>
                                <div class="col-sm-12 col-md-12 col-xs-12 course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
                                    <div class="os-resize-observer-host observed">
                                        <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
                                    </div>
                                    <div class="os-size-auto-observer observed"
                                         style="height: calc(100% + 1px); float: left;">
                                        <div class="os-resize-observer"></div>
                                    </div>

                                    <div class="os-content-glue"
                                         style="margin: 0px; height: 200px; width: 350px;"></div>
                                    <div class="os-padding px-3">
                                        <!-- os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid -->
                                        <div class="" style="overflow-y: auto;">
                                            <div class="os-content" style="padding: 0px; height: auto; width: 100%;">
                                                <div class="course-box-desc p-0">
                                                    <!--Essayer de jouer avec -->
                                                    <div class="no-overflow scrollable"
                                                         style="font-family: sans-serif; height: 180px;overflow-y: scroll"
                                                         id="style-1"><?= $c['summary'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable">
                                        <!--<div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div>-->
                                    </div>
                                    <!--<div class="os-scrollbar os-scrollbar-vertical">
                                        <div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div>
                                    </div>-->
                                    <div class="os-scrollbar-corner"></div>
                                </div>
                            </div>
                            <div class="">
                                <div class="course--btn"><a class="btn btn-danger w-100 font-weight-bold"
                                                            href="https://learning.iso.org/course/view.php?id=<?= $c['id'] ?>"
                                                            style="border-radius: 0px; font-family: sans-serif; background-color: #E60001">Get access</a>
                                </div>
                            </div>
                        </div>
                    </div>
		</div>
             <?php endif ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!-- STRATE 06 -->
<div class="jumbotron jumbotron-fluid" style="background-color: #ffffff;margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="pb-4" style="font-family: sans-serif;">MICROLEARNING VIDEOS</h4>
            </div>
        </div>
        <div class="row">
	    <?php foreach (getCategories("microlearning") as $k => $c): ?>
	     <?php if($c['visible']):  ?>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="
    padding-left: 0;
">
                    <div class="card-deck m-0 h-auto">
                        <div class="card c-course-box mb-5" data-courseid="2" data-type="1">
                            <div class="c-course-content row no-gutters p-0">
                                <div>
                                    <a href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                       class="col-12 align-self-start p-0">
                                        <!--<div class="course-box-icons-list position-absolute p-1 mt-2"
                                             style="right: 10%; background-color: rgba(0,0,0, .4)">
                                            <i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true"
                                               title="Self enrolment" aria-label="Self enrolment"></i>
                                        </div>-->
                                        <!--<div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div>-->
                                        <!-- <img src="<?php // getResizedImg($imgIsoOnline[$k]) ?>" class="img-fluid">-->
                                        <?php $url = "https://learning.iso.org" . getUrlOverviewFiles((int)$c['id']) ?>
                                        <img src="<?= getResizedImg($url) ?>" alt="" class="img-fluid">
                                    </a>
                                </div>

                                <div class="c-course-box-content w-100">
                                    <!-- Envevement de la scrollbar sur le titre du cours -->
                                    <!-- <div class="" style="height: 48px; overflow-x: hidden; overflow-y: scroll"> -->
                                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12"
                                         style="overflow-x: hidden; overflow-y: hidden; height: 80px">

                                        <h4 class="course-box-title font-weight-bold pt-3"
                                            style="font-family: sans-serif;">
                                        <span class="course-box-title-txt px-0 pt-2" title="<?= $c['fullname'] ?>">
                                            <a class=""
                                               href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                               style="color: #000"><?= $c['fullname'] ?></a>
                                        </span>
                                            <!--<div class="course-box-icons" style="top: 3%; left: 80%">
                                                <ul class="course-box-icons-list">
                                                    <li><i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul>
                                            </div>-->
                                        </h4>
                                    </div>
                                    <!--<div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 75px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc p-0"><div class="no-overflow"><?= $mc['summary'] ?></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end m-0"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=<?= $mc['id'] ?>" style="border-radius: 0px">Get access</a>-->
                                </div>
                                <div class="col-sm-12 col-md-12 col-xs-12 course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
                                    <div class="os-resize-observer-host observed">
                                        <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
                                    </div>
                                    <div class="os-size-auto-observer observed"
                                         style="height: calc(100% + 1px); float: left;">
                                        <div class="os-resize-observer"></div>
                                    </div>

                                    <div class="os-content-glue"
                                         style="margin: 0px; height: 200px; width: 350px;"></div>
                                    <div class="os-padding px-3">
                                        <!-- os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid -->
                                        <div class="" style="overflow-y: auto;">
                                            <div class="os-content" style="padding: 0px; height: auto; width: 100%;">
                                                <div class="course-box-desc p-0">
                                                    <!--Essayer de jouer avec -->
                                                    <div class="no-overflow scrollable"
                                                         style="font-family: sans-serif; height: 180px;overflow-y: scroll"
                                                         id="style-1"><?= $c['summary'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable">
                                        <!--<div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div>-->
                                    </div>
                                    <!--<div class="os-scrollbar os-scrollbar-vertical">
                                        <div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div>
                                    </div>-->
                                    <div class="os-scrollbar-corner"></div>
                                </div>
                            </div>
                            <div class="">
                                <div class="course--btn"><a class="btn btn-danger w-100 font-weight-bold"
                                                            href="https://learning.iso.org/course/view.php?id=<?= $c['id'] ?>"
                                                            style="border-radius: 0px; font-family: sans-serif; background-color: #E60001">Get access</a>
                                </div>
                            </div>
                        </div>
                    </div>
		</div>
	     <?php endif ?>	
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="jumbotron jumbotron-fluid" style="background-color: #ffffff;margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="pb-4" style="font-family: sans-serif;">THIRD-PARTY ELEARNING COURSES</h4>
            </div>
        </div>
        <div class="row">
	    <?php foreach (getCategories("Third-party eLearning courses") as $k => $c): ?>
              <?php if($c['visible']): ?>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="
    padding-left: 0;
">
                    <div class="card-deck m-0 h-auto">
                        <div class="card c-course-box mb-5" data-courseid="2" data-type="1">
                            <div class="c-course-content row no-gutters p-0">
                                <div>
                                    <a href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                       class="col-12 align-self-start p-0">
                                        <!--<div class="course-box-icons-list position-absolute p-1 mt-2"
                                             style="right: 10%; background-color: rgba(0,0,0, .4)">
                                            <i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true"
                                               title="Self enrolment" aria-label="Self enrolment"></i>
                                        </div>-->
                                        <!--<div class="card-img-top myoverviewimg courseimage" style="background-image: url(https://learning.iso.org/pluginfile.php/35/course/overviewfiles/5fiQu2oWJI9_80_DX3850_DY3850_CX2048_CY1367.jpg);"></div>-->
                                        <!-- <img src="<?php // getResizedImg($imgIsoOnline[$k]) ?>" class="img-fluid">-->
                                        <?php $url = "https://learning.iso.org" . getUrlOverviewFiles((int)$c['id']) ?>
                                        <img src="<?= getResizedImg($url) ?>" alt="" class="img-fluid">
                                    </a>
                                </div>

                                <div class="c-course-box-content w-100">
                                    <!-- Envevement de la scrollbar sur le titre du cours -->
                                    <!-- <div class="" style="height: 48px; overflow-x: hidden; overflow-y: scroll"> -->
                                    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12"
                                         style="overflow-x: hidden; overflow-y: hidden; height: 80px">

                                        <h4 class="course-box-title font-weight-bold pt-3"
                                            style="font-family: sans-serif;">
                                        <span class="course-box-title-txt px-0 pt-2" title="<?= $c['fullname'] ?>">
                                            <a class=""
                                               href="https://learning.iso.org/course/view.php?id=<?= (int)$c['id'] ?>"
                                               style="color: #000"><?= $c['fullname'] ?></a>
                                        </span>
                                            <!--<div class="course-box-icons" style="top: 3%; left: 80%">
                                                <ul class="course-box-icons-list">
                                                    <li><i class="icon fas fa-sign-in-alt fa-fw fa-3x" aria-hidden="true" title="Self enrolment" aria-label="Self enrolment"></i></li></ul>
                                            </div>-->
                                        </h4>
                                    </div>
                                    <!--<div class="course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px; height: 75px; width: 279px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px; height: auto; width: 100%;"><div class="course-box-desc p-0"><div class="no-overflow"><?= $mc['summary'] ?></div></div></div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div></div><div class="courses-view-course-item-footer col-12 align-self-end m-0"><div class="course--btn"><a class="btn btn-primary w-100" href="https://learning.iso.org/course/view.php?id=<?= $mc['id'] ?>" style="border-radius: 0px">Get access</a>-->
                                </div>
                                <div class="col-sm-12 col-md-12 col-xs-12 course-box-content-desc os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
                                    <div class="os-resize-observer-host observed">
                                        <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
                                    </div>
                                    <div class="os-size-auto-observer observed"
                                         style="height: calc(100% + 1px); float: left;">
                                        <div class="os-resize-observer"></div>
                                    </div>

                                    <div class="os-content-glue"
                                         style="margin: 0px; height: 200px; width: 350px;"></div>
                                    <div class="os-padding px-3">
                                        <!-- os-viewport os-viewport-native-scrollbars-invisible os-viewport-native-scrollbars-overlaid -->
                                        <div class="" style="overflow-y: auto;">
                                            <div class="os-content" style="padding: 0px; height: auto; width: 100%;">
                                                <div class="course-box-desc p-0">
                                                    <!--Essayer de jouer avec -->
                                                    <div class="no-overflow scrollable"
                                                         style="font-family: sans-serif; height: 180px;overflow-y: scroll"
                                                         id="style-1"><?= $c['summary'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable">
                                        <!--<div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div>-->
                                    </div>
                                    <!--<div class="os-scrollbar os-scrollbar-vertical">
                                        <div class="os-scrollbar-track os-scrollbar-track-off"><div class="os-scrollbar-handle" style="height: 21.8878%; transform: translate(0px, 0px);"></div></div>
                                    </div>-->
                                    <div class="os-scrollbar-corner"></div>
                                </div>
                            </div>
                            <div class="">
                                <div class="course--btn"><a class="btn btn-danger w-100 font-weight-bold"
                                                            href="https://learning.iso.org/course/view.php?id=<?= $c['id'] ?>"
                                                            style="border-radius: 0px; font-family: sans-serif; background-color: #E60001">Get access</a>
                                </div>
                            </div>
                        </div>
                    </div>
		</div>
             <?php endif  ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="container">
    <hr>
    <div class="row">
        <div class="text-center col-lg-6 offset-lg-3">
            <a href="https://learning.iso.org/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Flearning.iso.org"
               taget="_blank">Privacy Policy</a>
            <h6 class="mt-4">&copy; All Rights Reserved</h6>
            <p>"Change is the end result of all true learning."(Leo Buscaglia)</p>
        </div>
    </div>
</div>

<!-- -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.13.0/js/OverlayScrollbars.min.js"></script>
<!--<script src="js/jquery-3.4.1.min.js"></script>-->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/popper.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js"></script>
<!--<script src="js/bootstrap-4.4.1.js"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    $(function () {
        //The passed argument has to be at least a empty object or a object with your desired options
        $("body").overlayScrollbars({
            className            : "os-theme-dark",
            resize               : "none",
            sizeAutoCapable      : true,
            clipAlways           : true,
            normalizeRTL         : true,
            paddingAbsolute      : false,
            autoUpdate           : null,
            autoUpdateInterval   : 33,
            updateOnLoad         : ["img"],
            nativeScrollbarsOverlaid : {
                showNativeScrollbars   : false,
                initialize             : true
            },
            overflowBehavior : {
                x : "none",
                y : "scroll"
            },
            scrollbars : {
                visibility       : "auto",
                autoHide         : "never",
                autoHideDelay    : 800,
                dragScrolling    : true,
                clickScrolling   : false,
                touchSupport     : true,
                snapHandle       : false
            },
            textarea : {
                dynWidth       : false,
                dynHeight      : false,
                inheritedAttrs : ["style", "class"]
            },
            callbacks : {
                onInitialized               : null,
                onInitializationWithdrawn   : null,
                onDestroyed                 : null,
                onScrollStart               : null,
                onScroll                    : null,
                onScrollStop                : null,
                onOverflowChanged           : null,
                onOverflowAmountChanged     : null,
                onDirectionChanged          : null,
                onContentSizeChanged        : null,
                onHostSizeChanged           : null,
                onUpdated                   : null
            }
        });
    });
</script>
</body>
</html>
