<?php
require 'database.php';

$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

if (isset($_GET['q'])) {
    
    $query = $db->query("SELECT * FROM mdl_course c INNER JOIN mdl_course_categories cc ON c.category = cc.id WHERE c.fullname LIKE '%".$_GET['q']."%'");    
    $results = $query->fetchAll();
    $data = [];
    foreach ($results as $key => $r) {
        $data[] = [
            'id' => $r['id'],
            'fullname' => $r['fullname'],
            'summary' => $r['summary'],
            'img' => getUrlOverviewFiles($r['id']),
            'category' => $r['name']
        ];
    }
    echo json_encode($data);
}


function getUrlOverviewFiles($idcourse) {
    global $db;
    $query = $db->query("select cont.instanceid idCourse, f.filename filename, cont.id contexte
from mdl_context cont
inner join mdl_files f
on f.contextid = cont.id
where cont.instanceid = '".$idcourse."'
and f.component = 'course'
and f.filename <> '.'
order by f.id desc");
//$query->execute(array((int) $idcourse));
    $data = $query->fetch();
    return "/pluginfile.php/".$data['contexte']."/course/overviewfiles/".$data['filename'];
}