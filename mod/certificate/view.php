<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles viewing a certificate
 *
 * @package    mod_certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("$CFG->dirroot/mod/certificate/locallib.php");
require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
require_once("$CFG->libdir/pdflib.php");

$id = required_param('id', PARAM_INT);    // Course Module ID
$action = optional_param('action', '', PARAM_ALPHA);
$edit = optional_param('edit', -1, PARAM_BOOL);

if (!$cm = get_coursemodule_from_id('certificate', $id)) {
    print_error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id'=> $cm->course))) {
    print_error('course is misconfigured');
}
if (!$certificate = $DB->get_record('certificate', array('id'=> $cm->instance))) {
    print_error('course module is incorrect');
}

require_login($course, false, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/certificate:view', $context);

$event = \mod_certificate\event\course_module_viewed::create(array(
    'objectid' => $certificate->id,
    'context' => $context,
));
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('certificate', $certificate);
$event->trigger();

$completion=new completion_info($course);
$completion->set_module_viewed($cm);

// Initialize $PAGE, compute blocks
$PAGE->set_url('/mod/certificate/view.php', array('id' => $cm->id));
$PAGE->set_context($context);
$PAGE->set_cm($cm);
$PAGE->set_title(format_string($certificate->name));
$PAGE->set_heading(format_string($course->fullname));

if (($edit != -1) and $PAGE->user_allowed_editing()) {
     $USER->editing = $edit;
}

// Add block editing button
if ($PAGE->user_allowed_editing()) {
    $editvalue = $PAGE->user_is_editing() ? 'off' : 'on';
    $strsubmit = $PAGE->user_is_editing() ? get_string('blockseditoff') : get_string('blocksediton');
    $url = new moodle_url($CFG->wwwroot . '/mod/certificate/view.php', array('id' => $cm->id, 'edit' => $editvalue));
    $PAGE->set_button($OUTPUT->single_button($url, $strsubmit));
}

// Check if the user can view the certificate
if ($certificate->requiredtime && !has_capability('mod/certificate:manage', $context)) {
    if (certificate_get_course_time($course->id) < ($certificate->requiredtime * 60)) {
        $a = new stdClass;
        $a->requiredtime = $certificate->requiredtime;
        notice(get_string('requiredtimenotmet', 'certificate', $a), "$CFG->wwwroot/course/view.php?id=$course->id");
        die;
    }
}

// Create new certificate record, or return existing record
$certrecord = certificate_get_issue($course, $USER, $certificate, $cm);

make_cache_directory('tcpdf');

// Load the specific certificate type.
require("$CFG->dirroot/mod/certificate/type/$certificate->certificatetype/certificate.php");

if (!isset($_GET['action']) && isset($_GET['id'])) {
    try {
            require_once 'src/PHPMailer.php';

    
            $mail = new PHPMailer\PHPMailer\PHPMailer();

    $to = $USER->email;
    //$to = "houessinonlandry@gmail.com";
    $mail->isSmtp();
    //$mail->Host = "smtp.gmail.com";
    $mail->Host = "email-smtp.eu-west-3.amazonaws.com";
    $mail->Port = 587;
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->SMTPAuth = true;
    //$mail->Username = 'infoiso.learning@gmail.com';
    $mail->Username = 'AKIAVL5CVGNKC76NW536';
    $mail->Password = 'BGntditQBfy659RfN7F8QRtO53EST3KN2dYge/vlEPla';

    //$mail->Password = "@Azerty21";
        //$mail->setFrom("noreply@learning.iso.org", "NoReply");
    $mail->setFrom("noreply@learning.iso.org", "NoReply");
    //$mail->setFrom("infoiso.learning@gmail.com", "noreply@learning.iso.org");
    $mail->addReplyTo("noreply@learning.iso.org", "Do not reply to this email");
    $mail->SMTPDebug = false;
    $mail->addAddress($to, $USER->firstname.' '.$USER->lastname);
    $mail->Subject = "We need your feedback - complete the end of eLearning course survey";
    //$mail->Subject = "$course->fullname;"
    $mail->isHTML(true);
    $mail->CharSet = 'utf-8';

    $mail->Body = "<html><body><h4>Dear Participant, </h4>
    <p>Thank you very much for completing/watching the eLearning course/microlearning video through the ISO digital learning platform.</p>
<p>ISO puts great emphasis on improving continually its digital training activities. Therefore, we would be grateful if could complete the satisfaction survey that will help us to check whether or not we have been able to meet your learning and development needs. It takes 5 minutes to complete it.</p>
<p>Please click on <b>one of the links</b> listed below to access the survey and complete it in less than 5 mins.</p>
<p>
    <ul>
        <li><a href='https://www.smartsurvey.co.uk/s/eLearningPM/''>https://www.smartsurvey.co.uk/s/eLearningPM/</a> (only for the ISO eLearning course on Project Management for Committee Managers that have you just completed).</li>
        <li><a href='https://www.smartsurvey.co.uk/s/third-partycourses/''>  https://www.smartsurvey.co.uk/s/third-partycourses/</a> (only for the third-party eLearning course or ISO microlearning course that have you just completed). </li>
    </ul>
</p>
<p>
    Alternatively, you can copy and paste one of the above-listed links into your internet browser and complete the survey.
</p>
<p>
    Should you have any questions about the survey or in case you encounter any difficulty, please contact us at <a href='mailto:capacity@iso.org'>capacity@iso.org</a>
</p>
<p>Thank you again for your participation and we look forward to your valuable feedback.</p>
<p><b>Best regards</b></p>
<p><b>ISO capacity building team </b></p></body></html>";

    //$mail->Body = "<html><body><h1>je teste les mails</h1></body></html>";

    if($mail->send()) {
        // Success! Redirect to a thank you page. Use the
        // POST/REDIRECT/GET pattern to prevent form resubmissions
        // when a user refreshes the page.

        echo '';
    }
    else {
        //var_dump("Error: " . $mail->ErrorInfo);
        echo '';
    }
    }
     catch (Exception $e) {

    }
}

if (empty($action)) { // Not displaying PDF
    echo $OUTPUT->header();

    $viewurl = new moodle_url('/mod/certificate/view.php', array('id' => $cm->id));
    groups_print_activity_menu($cm, $viewurl);
    $currentgroup = groups_get_activity_group($cm);
    $groupmode = groups_get_activity_groupmode($cm);

    if (has_capability('mod/certificate:manage', $context)) {
        $numusers = count(certificate_get_issues($certificate->id, 'ci.timecreated ASC', $groupmode, $cm));
        $url = html_writer::tag('a', get_string('viewcertificateviews', 'certificate', $numusers),
            array('href' => $CFG->wwwroot . '/mod/certificate/report.php?id=' . $cm->id));
        echo html_writer::tag('div', $url, array('class' => 'reportlink'));
    }
//
    if (!empty($certificate->intro)) {
        echo $OUTPUT->box(format_module_intro('certificate', $certificate, $cm->id), 'generalbox', 'intro');
    }

    if ($attempts = certificate_get_attempts($certificate->id)) {
        echo certificate_print_attempts($course, $certificate, $attempts);
    }
    if ($certificate->delivery == 0)    {
        $str = get_string('openwindow', 'certificate');
    } elseif ($certificate->delivery == 1)    {
        $str = get_string('opendownload', 'certificate');
    } elseif ($certificate->delivery == 2)    {
        $str = get_string('openemail', 'certificate');
    }
    echo html_writer::tag('p', $str, array('style' => 'text-align:center'));
    $linkname = get_string('getcertificate', 'certificate');

    $link = new moodle_url('/mod/certificate/view.php?id='.$cm->id.'&action=get');
    $button = new single_button($link, $linkname);
    if ($certificate->delivery != 1) {
        $button->add_action(new popup_action('click', $link, 'view' . $cm->id, array('height' => 600, 'width' => 800)));

    }

    echo html_writer::tag('div', $OUTPUT->render($button), array('style' => 'text-align:center'));
    echo $OUTPUT->footer($course);
    exit;
} else { // Output to pdf

    // No debugging here, sorry.
    $CFG->debugdisplay = 0;
    @ini_set('display_errors', '0');
    @ini_set('log_errors', '1');

    $filename = certificate_get_certificate_filename($certificate, $cm, $course) . '.pdf';

    // PDF contents are now in $file_contents as a string.
    $filecontents = $pdf->Output('', 'S');

    if ($certificate->savecert == 1) {
        certificate_save_pdf($filecontents, $certrecord->id, $filename, $context->id);
    }

    if ($certificate->delivery == 0) {
        // Open in browser.
        send_file($filecontents, $filename, 0, 0, true, false, 'application/pdf');
    } elseif ($certificate->delivery == 1) {
        // Force download.
        send_file($filecontents, $filename, 0, 0, true, true, 'application/pdf');
    } elseif ($certificate->delivery == 2) {
        certificate_email_student($course, $certificate, $certrecord, $context, $filecontents, $filename);
        // Open in browser after sending email.
        send_file($filecontents, $filename, 0, 0, true, false, 'application/pdf');
    }
}
