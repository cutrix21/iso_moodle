<?php

class email_control {
  
  private $data = [];
  private $file_name = "";

  function __construct($file_name) {
    $this->file_name = $file_name;
    $this->data = file_get_contents($file_name);
  }

  function getData() {
    return unserialize($this->data);
  }

  function userHasNewForCourse($username, $course_name) {
    $data = [];
    if ($this->data === "") {
      $dataCourse = [
        "username" => $username,
        "course_name" => $course_name,
        "is_send" => false
      ];

       $data[] = $dataCourse;
        file_put_contents($this->file_name, serialize($data));

    } else {  
        foreach ($this->getData() as $d) {
          if ($d['course_name'] !== $course_name && $d['username'] === $username) {
             $datas = $this->getData();
             $d['course_name'] = $course_name;
             $d['is_send'] = false;
             $datas[] = $d;
             file_put_contents("check_mail", serialize($datas));
             return true;
          }
        }
        return false;
    } 
  }

  function setIsSend($user, $course) {
    $item = [];
    $index = null;
    foreach ($this->getData() as $key => $d) {
      if ($d['username'] === $user && $d['course_name'] === $course) {
        $d['is_send'] = true;
        $item = $d;      
        $index = $key;
        $datas = $this->getData();
        $datas[$index] = $item;
        file_put_contents("check_mail", serialize($datas));
      }
    }
    var_dump("datas ==> ".$this->getData());
  }

  function emailHasSend($user, $course) {
    foreach ($this->getData() as $key => $d) {
      if ($d['username'] === $user && $d['course_name'] === $course) {
        return $d['is_send'] === true;
      }
    }
  }


  function addNewData() {

  }

  function setIsSendEmail($user, $status = false) {
    
  }

  function getLastCourseUSer($user) {

  }
}