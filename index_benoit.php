<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISO</title>

    <!-- Bootstrap -->
    <!--<link href="css/bootstrap-4.4.1.css" rel="stylesheet">-->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- All -->
    <link href="custom.css" rel="stylesheet">
</head>
<body>
<!-- STRATE 01 -->
<div class="container">
    <div class="row text-right pt-3 pb-3">
        <div class="col-12">
            <a href="#" class="login-btn"
               style="background-color: #E60100 !important; border-radius: 0px; border: 0px solid transparent !important; font-family: 'MetaCompPro-Normal', serif">Log
                in</a>
        </div>
    </div>
</div>
<!-- -->
<hr style="margin-top: 0rem; margin-bottom: 0rem;">
<!-- STRATE 02 -->
<div class="container">
    <div class="row text-left pt-4 pb-4">
        <div class="col-12">
            <div class="media" style="align-items: center;">
                <img class="align-self-center mr-4"
                     src="https://learning.iso.org/pluginfile.php/1/core_admin/logo/0x300/1601276563/Logo.png"
                     alt="ISO Learning" style="max-width:100px;">
                <div class="media-body">
                    <h1 class="mt-0">DIGITAL LEARNING PLATFORM</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- STRATE 03 -->

<div class="jumbotron jumbotron-fluid text-center" style="background-color: #f7f7f7; margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 align-top d-flex justify-content-center">
                <input type="search" placeholder="Search Learning Solutions" class="align-top form-control"
                       style="display:inline; width:calc(99% - 50px); height:52px; border-radius: 0px;">
                <button class="align-top btn btn-secondary" type="submit" id="go"
                        style="background-color: #E60100; border-radius: 0px; border: 0px solid transparent; font-family: 'MetaCompPro-Normal', serif; display:inline; width:50px; height:51px;">
                    Go
                </button>
            </div>
        </div>
    </div>
</div>

<!-- STRATE 04 -->

<div class="container">
    <div class="row pt-4 pb-4">
        <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <h4>LEARN ONLINE WITH ISO</h4>
            <p>The ISO digital learning platform offers a wide range of digital learning solutions in an effort to
                provide the learners with a great deal of flexibility, allowing them to study at any time from any place
                at their own convenient speed without worrying about timetables and schedules.
                Unlike traditional classroom learning opportunities, online learning environments foster additional
                learning experiences where learners can interact, collaborate and retain information in a far better
                manner. </p>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <h4>HOW TO REGISTER</h4>
            <p>If you have already an account on the ISO Global Directory, you could access directly the digital
                learning platform using that account after accepting the related terms and conditions. Otherwise, you
                need to contact the ISO member in your country to create you an account before completing the
                registration process.
                <br/><br/>
                For more information, please contact khammash@iso.org </p>
        </div>
    </div>
</div>
<!-- STRATE 05 -->
<div class="jumbotron jumbotron-fluid" style="background-color: #f7f7f7;margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="pb-4">ELEARNING COURSES</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/01.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">Project Management in the ISO environment</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This e-learning course you the Committee Manager, and support team, to
                            successfully deliver high quality documents in a timely manner. It helps everyone in the ISO
                            Community be effective and efficient when the availability of resources is a challenge:</p>
                        <ul>
                            <li>Planning: Identifying what matters when scheduling the project development, and when
                                setting realistic target dates for your projects
                            </li>
                            <li>Monitoring: Understanding the tools and solutions available to you, and defining your
                                own strategy in terms of pro-activity
                            </li>
                            <li>Working together: Understanding and providing the appropriate support to the key roles
                                in project development (Convenors and project leaders)
                            </li>
                        </ul>
                        <p class="card-text"><b>Language</b> English</p>
                        <p class="card-text"><b>Delivery method</b> Self-paced course</p>
                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/image.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">Building blocks of ISO system</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This microlearning course includes three short videos about the
                            International Organization for Standardization (ISO), membership at ISO technical committees
                            as well as the principles of international standards development. </p>

                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/TCs.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">ISO TCs, SCs and WGs</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This microlearning course includes short videos about ISO technical
                            committees (TCs) subcommittees (SCs) and working groups (WGs) as well as national mirror
                            committees (NMCs).</p>
                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
        </div>
    </div>
</div>

<!-- STRATE 06 -->
<div class="jumbotron jumbotron-fluid" style="background-color: #ffffff;margin-bottom:0rem;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="pb-4">MICROLEARNING VIDEOS</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/01.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">Project Management in the ISO environment</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This e-learning course you the Committee Manager, and support team, to
                            successfully deliver high quality documents in a timely manner. It helps everyone in the ISO
                            Community be effective and efficient when the availability of resources is a challenge:</p>
                        <ul>
                            <li>Planning: Identifying what matters when scheduling the project development, and when
                                setting realistic target dates for your projects
                            </li>
                            <li>Monitoring: Understanding the tools and solutions available to you, and defining your
                                own strategy in terms of pro-activity
                            </li>
                            <li>Working together: Understanding and providing the appropriate support to the key roles
                                in project development (Convenors and project leaders)
                            </li>
                        </ul>
                        <p class="card-text"><b>Language</b> English</p>
                        <p class="card-text"><b>Delivery method</b> Self-paced course</p>
                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/image.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">Building blocks of ISO system</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This microlearning course includes three short videos about the
                            International Organization for Standardization (ISO), membership at ISO technical committees
                            as well as the principles of international standards development. </p>

                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="card mb-4" style="height:calc(100% - 1.5rem);">
                    <img class="card-img-top" src="https://learning.iso.org/resize_img/TCs.jpg" alt="Card image cap">
                    <div class="card-header">
                        <h4 class="card-title">ISO TCs, SCs and WGs</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">This microlearning course includes short videos about ISO technical
                            committees (TCs) subcommittees (SCs) and working groups (WGs) as well as national mirror
                            committees (NMCs).</p>
                        <a href="#" class="btn btn-primary w-100 btn_get_access">Go somewhere</a>
                    </div>
                </div>
            </div>
            <!-- -->
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="container">
    <hr>
    <div class="row">
        <div class="text-center col-lg-6 offset-lg-3">
            <a href="https://learning.iso.org/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Flearning.iso.org"
               taget="_blank">Privacy Policy</a>
            <h6 class="mt-4">&copy; All Rights Reserved</h6>
            <p>"Change is the end result of all true learning."(Leo Buscaglia)</p>
        </div>
    </div>
</div>

<!-- -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.4.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap-4.4.1.js"></script>
</body>
</html>